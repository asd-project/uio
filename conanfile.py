import os

from conans import ConanFile, CMake

project_name = "uio"


class UioConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Generic input/output components for GUI interaction"
    topics = ("asd", "ui", "io")
    generators = "cmake"
    settings = "os", "compiler", "build_type", "arch"
    exports_sources = "include*", "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.algorithm/0.0.1@asd/testing",
        "asd.math/0.0.1@asd/testing",
        "asd.flow/0.0.1@asd/testing",
        "asd.signal/0.0.1@asd/testing"
    )

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = [project_name]
