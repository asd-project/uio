//---------------------------------------------------------------------------

#include <uio/mouse_manipulator.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace uio
    {
        mouse_manipulator::mouse_manipulator(uio::mouse_input & input, uio::mouse_output & output) :
            _input(input),
            _output(output),
            _observer(input.on_move, [this](const uio::mouse_move_event & e) {
                if (_enabled) {
                    add_delta(e.delta);
                }
            })
        {}

        mouse_manipulator::~mouse_manipulator() {
            disable();
        }

        math::float_point mouse_manipulator::fetch_delta() {
            if (!_enabled) {
                return {0.0f, 0.0f};
            }

            auto delta = std::exchange(_delta, {0.0f, 0.0f});
            _output.reset_position();

            return delta;
        }

        void mouse_manipulator::enable() {
            if (!_enabled) {
                toggle();
            }
        }

        void mouse_manipulator::disable() {
            if (_enabled) {
                toggle();
            }
        }

        void mouse_manipulator::toggle() {
            _enabled = !_enabled;

            if (_enabled) {
                _start_point = _input.position();
                _output.reset_position();
                _output.set_relative_mode(true);
            } else {
                _output.set_relative_mode(false);
                _output.set_position(_start_point);
            }

            _delta = {0.0f, 0.0f};
        }

        void mouse_manipulator::add_delta(const math::float_point & delta) {
            _delta += delta;
        }
    }
}

//---------------------------------------------------------------------------
