//---------------------------------------------------------------------------

#include <uio/keyboard_manipulator.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace uio
    {
        constexpr asd::enum_index_array<uio::movement_layout_preset, uio::movement_layout> presets{{{
            {{uio::scancode::up, uio::scancode::left, uio::scancode::down, uio::scancode::right}},
            {{uio::scancode::w, uio::scancode::a, uio::scancode::s, uio::scancode::d}}
        }}};

        keyboard_manipulator::keyboard_manipulator(const uio::keyboard_input & keyboard) : _keyboard(keyboard) {}

        math::point<float> keyboard_manipulator::delta(float amplitude, uio::movement_layout_preset layout) const {
            return delta(amplitude, presets[layout]);
        }

        math::point<float> keyboard_manipulator::delta(float amplitude, const uio::movement_layout & layout) const {
            math::point<float> result;

            if (_keyboard.is_pressed(layout[uio::direction::up])) {
                result.y += 1.0f;
            }

            if (_keyboard.is_pressed(layout[uio::direction::left])) {
                result.x -= 1.0f;
            }

            if (_keyboard.is_pressed(layout[uio::direction::down])) {
                result.y -= 1.0f;
            }

            if (_keyboard.is_pressed(layout[uio::direction::right])) {
                result.x += 1.0f;
            }

            float length = math::sqrt(result.x * result.x + result.y * result.y);
            return length > 0 ? result * (amplitude / length) : result;
        }
    }
}

//---------------------------------------------------------------------------
