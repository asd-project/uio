//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <signal/member_source.h>

#include <uio/keyboard/keycode.h>
#include <uio/keyboard/keymod.h>
#include <uio/keyboard/scancode.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace uio
    {
        class keyboard_input;

        struct keyboard_event
        {
            keycode key;
            scancode code;
            keymods modifiers;
            keyboard_input & keyboard;
        };

        class keyboard_input
        {
        public:
            using allocator_type = pmr::polymorphic_allocator<keyboard_input>;

            keyboard_input(const allocator_type & alloc = {}) :
                on_press(alloc),
                on_release(alloc)
            {}

            keyboard_input(const keyboard_input &) = delete;
            keyboard_input & operator = (const keyboard_input &) = delete;

            virtual ~keyboard_input() {}

            virtual bool is_pressed(uio::keycode keycode) const = 0;
            virtual bool is_pressed(uio::scancode scancode) const = 0;
            virtual uio::keymods modifiers() const = 0;

            signal::member_source<keyboard_input, void(const keyboard_event &)> on_press;
            signal::member_source<keyboard_input, void(const keyboard_event &)> on_release;

        protected:
            void process_press(const keyboard_event & e) { on_press(e); }
            void process_release(const keyboard_event & e) { on_release(e); }
        };
    }
}
