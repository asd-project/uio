//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/common.h>
#include <meta/flag.h>
#include <math/point.h>

#include <signal/member_source.h>

//---------------------------------------------------------------------------


namespace asd
{
    namespace uio
    {
        class mouse_input;

        enum class mouse_button : u8
        {
            left,
            middle,
            right,
            x1,
            x2
        };

        adapt_enum_flags(mouse_button);

        using mouse_buttons = enum_flags<mouse_button>;

        struct mouse_move_event
        {
            math::float_point delta;
            mouse_input & mouse;
        };

        struct mouse_button_event
        {
            mouse_button button;
            mouse_input & mouse;
        };

        struct mouse_wheel_event
        {
            math::float_point delta;
            mouse_input & mouse;
        };

        struct touch_event
        {
            mouse_input & mouse;
            int touch_count = 0;
        };

        class mouse_input
        {
        public:
            using allocator_type = pmr::polymorphic_allocator<mouse_input>;

            mouse_input(const allocator_type & alloc = {}) :
                on_move(alloc),
                on_press(alloc),
                on_release(alloc),
                on_wheel(alloc),
                on_touch_start(alloc),
                on_touch_end(alloc)
            {}

            mouse_input(const mouse_input &) = delete;
            mouse_input & operator = (const mouse_input &) = delete;

            virtual ~mouse_input() {}

            bool is_pressed() const {
                auto pressed_buttons = this->buttons();
                return pressed_buttons.any();
            }

            bool is_pressed(uio::mouse_buttons buttons) const {
                auto pressed_buttons = this->buttons();
                return pressed_buttons.check(buttons);
            }

            virtual math::float_point position() const = 0;
            virtual mouse_buttons buttons() const = 0;

            signal::member_source<mouse_input, void(const mouse_move_event &)> on_move;
            signal::member_source<mouse_input, void(const mouse_button_event &)> on_press;
            signal::member_source<mouse_input, void(const mouse_button_event &)> on_release;
            signal::member_source<mouse_input, void(const mouse_wheel_event &)> on_wheel;
            signal::member_source<mouse_input, void(const touch_event &)> on_touch_start;
            signal::member_source<mouse_input, void(const touch_event &)> on_touch_end;

        protected:
            void process_move(const mouse_move_event & e) { on_move(e); }
            void process_press(const mouse_button_event & e) { on_press(e); }
            void process_release(const mouse_button_event & e) { on_release(e); }
            void process_wheel(const mouse_wheel_event & e) { on_wheel(e); }
            void process_touch_start(const touch_event & e) { on_touch_start(e); }
            void process_touch_end(const touch_event & e) { on_touch_end(e); }
        };

        class mouse_output
        {
        public:
            virtual ~mouse_output() {}

            virtual void set_position(const math::float_point & pos) = 0;
            virtual void reset_position() = 0;
            virtual void set_relative_mode(bool enabled) = 0;
        };
    }
}
