//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <uio/keyboard.h>

#include <container/span.h>
#include <container/map.h>

#include <signal/hub.h>
#include <signal/source.h>

#include <compare>

//---------------------------------------------------------------------------

namespace asd::uio
{
    using namespace std::string_view_literals;

    class shortcut_parse_exception : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };

    struct shortcut
    {
        constexpr shortcut(uio::scancode code, uio::keymods modifiers = 0) noexcept : modifiers(modifiers), code(code) {}
        constexpr shortcut(const char * s) : shortcut(parse(s)) {}
        constexpr shortcut(std::string_view s) : shortcut(parse(s)) {}

        constexpr shortcut(const shortcut &) noexcept = default;
        constexpr shortcut & operator = (const shortcut &) noexcept = default;

        constexpr friend std::strong_ordering operator <=> (const shortcut &, const shortcut &) noexcept = default;

        static constexpr shortcut parse(std::string_view s, std::string_view separator = "+"sv);

        uio::keymods modifiers;
        uio::scancode code;
    };
}

namespace std
{
    template <>
    struct hash<::asd::uio::shortcut>
    {
        constexpr size_t operator()(const ::asd::uio::shortcut & shortcut) const {
            return ::asd::hash_combine(std::pair{shortcut.modifiers, shortcut.code});
        }
    };
}

namespace asd::uio
{
    class shortcut_context
    {
    public:
        using allocator_type = pmr::polymorphic_allocator<shortcut_context>;

        shortcut_context(uio::keyboard_input & keyboard, const allocator_type & alloc = {}) :
            shortcut_context(keyboard, signal::global_hub(), alloc)
        {}

        shortcut_context(uio::keyboard_input & keyboard, signal::hub & hub, const allocator_type & alloc = {}) :
            _hub(hub),
            _signals(alloc)
        {
            _hub.subscribe(keyboard.on_release, [this](const keyboard_event & e) {
                for (auto it = _signals.begin(); it != _signals.end(); ++it) {
                    auto & shortcut = it.key();

                    if (e.modifiers == shortcut.modifiers && e.code == shortcut.code) {
                        it.value()(shortcut);
                    }
                }
            });
        }

        template <class F>
            requires(std::invocable<F, const uio::shortcut &>)
        auto subscribe(const uio::shortcut & shortcut, F && callback) {
            auto it = _signals.find(shortcut);

            if (it == _signals.end()) {
                it = _signals.try_emplace(shortcut).first;
            }

            return _hub.subscribe(it.value(), callback);
        }

        template <class F>
            requires(std::invocable<F, const uio::shortcut &>)
        void subscribe(const span<uio::shortcut> & shortcuts, F callback) {
            for (auto & shortcut : shortcuts) {
                subscribe(shortcut, callback);
            }
        }

        void unsubscribe(uint32 subscription) {
            _hub.unsubscribe(subscription);
        }

    private:
        signal::hub & _hub;
        asd::pmr::map<uio::shortcut, signal::source<void(const uio::shortcut &)>> _signals;
    };

    template <class T> requires (std::is_same_v<T, uio::keymod> || std::is_same_v<T, uio::keymods>)
    constexpr shortcut operator + (uio::scancode code, T modifier) {
        return {code, modifier};
    }

    template <class T> requires (std::is_same_v<T, uio::keymod> || std::is_same_v<T, uio::keymods>)
    constexpr shortcut operator + (T modifier, uio::scancode code) {
        return {code, modifier};
    }

    template <class T> requires (std::is_same_v<T, uio::keymod> || std::is_same_v<T, uio::keymods>)
    constexpr shortcut operator + (const shortcut & s, T modifier) {
        return {s.code, s.modifiers | modifier};
    }

    template <class T> requires (std::is_same_v<T, uio::keymod> || std::is_same_v<T, uio::keymods>)
    constexpr shortcut operator + (T modifier, const shortcut & s) {
        return {s.code, s.modifiers | modifier};
    }

    constexpr std::array<shortcut, 2> operator | (const shortcut & a, const shortcut & b) {
        return {a, b};
    }

    template <size_t N>
    constexpr std::array<shortcut, N + 1> operator | (const std::array<shortcut, N> & shortcuts, const shortcut & s) {
        return [&]<size_t ... Index>(std::index_sequence<Index...>) -> std::array<shortcut, N + 1> {
            return {shortcuts[Index]..., s};
        }(std::make_index_sequence<N>{});
    }

    template <size_t N>
    constexpr std::array<shortcut, N + 1> operator | (const shortcut & s, const std::array<shortcut, N> & shortcuts) {
        return [&]<size_t ... Index>(std::index_sequence<Index...>) -> std::array<shortcut, N + 1> {
            return {s, shortcuts[Index]...};
        }(std::make_index_sequence<N>{});
    }

    constexpr shortcut shortcut::parse(std::string_view s, std::string_view separator) {
        using namespace std::string_literals;
        using asd::operator +;

        size_t start_pos = 0;
        size_t separator_pos = s.find(separator);

        if (separator_pos == std::string::npos) {
            auto scan_code = parse_scancode(s);

            if (!scan_code) {
                BOOST_THROW_EXCEPTION(shortcut_parse_exception("Incorrect scan code name: "s + s));
            }

            return {*scan_code};
        }

        uio::keymods mods;

        do {
            auto keymod = parse_keymod(s.substr(start_pos, separator_pos));

            if (!keymod) {
                BOOST_THROW_EXCEPTION(shortcut_parse_exception("Incorrect key modifier name: "s + s.substr(start_pos, separator_pos)));
            }

            mods.set(*keymod);

            start_pos = separator_pos + 1;
            separator_pos = s.find(separator, start_pos);
        } while(separator_pos != std::string::npos);

        auto scan_code = parse_scancode(s.substr(start_pos));

        if (!scan_code) {
            BOOST_THROW_EXCEPTION(shortcut_parse_exception("Incorrect scan code name: "s + s.substr(start_pos)));
        }

        return {*scan_code, mods};
    }

    namespace literals
    {
        constexpr uio::shortcut operator ""_shortcut(const char * s, size_t size) {
            return uio::shortcut::parse({s, size});
        }
    }
}
