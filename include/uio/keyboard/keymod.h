//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/common.h>
#include <meta/map.h>
#include <meta/flag.h>

#include <algorithm/string.h>
#include <optional>
#include <string_view>

//---------------------------------------------------------------------------

namespace asd
{
    namespace uio
    {
        enum class keymod : u8
        {
            ctrl,
            alt,
            shift,
            gui,            // OS-specific modifier
            cmd = gui,      // MacOS special modifier
            win = gui       // Windows special modifier
        };

        adapt_enum_flags(keymod);

        using keymods = enum_flags<keymod>;

        constexpr auto keymod_strings = meta::make_map<std::string_view, uio::keymod, case_insensitive_less>({
            {"ctrl", uio::keymod::ctrl},
            {"alt", uio::keymod::alt},
            {"shift", uio::keymod::shift},
            {"gui", uio::keymod::gui},
            {"cmd", uio::keymod::cmd},
            {"win", uio::keymod::win}
        });

        constexpr std::optional<uio::keymod> parse_keymod(std::string_view s) {
            auto it = keymod_strings.find(s);
            return it != keymod_strings.end() ? it->second : std::optional<uio::keymod>{};
        }
    }
}
